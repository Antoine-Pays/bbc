#ifndef JOLIESYNTAXE_H
#define JOLIESYNTAXE_H
#include <QSyntaxHighlighter>
#include <QRegularExpression>
#include <QQuickTextDocument>
#include <cstdio>


class JolieSyntaxe : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    JolieSyntaxe(QTextDocument *parent = nullptr);
    Q_INVOKABLE void setDocument(QQuickTextDocument *pDoc);
protected:
    void highlightBlock(const QString &text) override;
    void colorer(const QString &text, int startIndex, int tailleText);

private:
    struct HighlightingRule
    {
        QRegularExpression pattern;
        QTextCharFormat format;
    };


    QVector<HighlightingRule> highlightingRules;

    QTextCharFormat emailFormat;
    QTextCharFormat httpFormat;

};

#endif // JOLIESYNTAXE_H
