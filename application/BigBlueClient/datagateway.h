#ifndef DATAGATEWAY_H
#define DATAGATEWAY_H

#include <QObject>

#include "serverSrc/server.h"

class DataGateway
{
public:
    DataGateway();

    std::list<Server*> getStub();
};

#endif // DATAGATEWAY_H
