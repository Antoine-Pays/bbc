#ifndef MANAGER_H
#define MANAGER_H

#include "serverSrc/serverlist.h"
#include "serverSrc/server.h"

#include "userSrc/userlist.h"

#include "datagateway.h"

class Manager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(ServerList* serverList READ serverList WRITE setServerList NOTIFY serverListChanged)
    Q_PROPERTY(Server* currentServer READ currentServer WRITE setCurrentServer NOTIFY currentServerChanged)

    Q_PROPERTY(RoomList* currentRoomList READ currentRoomList WRITE setCurrentRoomList NOTIFY currentRoomListChanged)
    Q_PROPERTY(Room* currentRoom READ currentRoom WRITE setCurrentRoom NOTIFY currentRoomChanged)

    Q_PROPERTY(MessageList* currentMessageList READ currentMessageList WRITE setCurrentMessageList NOTIFY currentMessageListChanged)
    Q_PROPERTY(UserList* currentUserList READ currentUserList WRITE setCurrentUserList NOTIFY currentUserListChanged)


    ServerList* m_serverList;
    Server* m_currentServer;

    RoomList* m_currentRoomList;
    Room* m_currentRoom;

    MessageList* m_currentMessageList;
    UserList* m_currentUserList;

    DataGateway m_dataGW;

public:
    Manager(QObject *parent=nullptr);

    ServerList* serverList() const;
    Server* currentServer() const;

    RoomList* currentRoomList() const;
    Room* currentRoom() const;

    MessageList* currentMessageList() const;
    UserList* currentUserList() const;


public slots:
    void setServerList(ServerList* serverList);
    void setCurrentServer(Server* currentServer);
    void setCurrentServer2(QString nom, QString lien);
    void appendServer(QString nom, QString lien){
        std::list<User*>u2{new User(QUrl("qrc:/image/logo.png"),"alexei2")};
        std::list<Message*>m2{new Message("content 2",new User(QUrl("qrc:/image/logo.png"),"timothe"), QDateTime(QDateTime::currentDateTime()))};
        std::list<Room*>r2{new Room("nom2", new MessageList(m2), new UserList(u2))};
        serverList()->append(new Server(lien,nom, new RoomList(r2)));
    }
    void removeServer(int index){
        serverList()->removeItem(index);
    }

    void setCurrentRoomList(RoomList* currentRoomList);
    void setCurrentRoom(Room* currentRoom);

    void setCurrentMessageList(MessageList* currentMessageList);
    void setCurrentUserList(UserList* currentUserList);

signals:
    void serverListChanged(ServerList* serverList);
    void currentServerChanged(Server* currentServer);

    void currentRoomListChanged(RoomList* currentRoomList);
    void currentRoomChanged(Room* currentRoom);

    void currentMessageListChanged(MessageList* currentMessageList);
    void currentUserListChanged(UserList* currentUserList);

};

#endif // MANAGER_H
