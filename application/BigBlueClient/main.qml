import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import QtQuick.Controls 2.0

import "serverMaster"
import "serverDetail"
import "present"

ApplicationWindow {
    id: root
    visible: true
    title: qsTr("BigBlueClient")
    property int svWidth : 150
    property int chatWidth: 350
    width: 1000
    minimumWidth: 600
    height: 600
    minimumHeight: 300

    Pannel{
        width: svWidth
        height: 200
        id:nav
    }

    ConnectedUserList{
        anchors.top: nav.bottom
        height: parent.height-nav.height
        width: svWidth
    }

    Chat{
        anchors.left:nav.right
        id:msgList
        width:chatWidth
        height:parent.height
    }

    Presentation{
        id : presentation
        anchors.left: msgList.right
        width:parent.width-500
        height: parent.height
    }

    Cam {
        id : cam
        anchors.left: msgList.right
        width:parent.width-500
        height: parent.height
        visible:false
    }


    Button {
        anchors.horizontalCenter: cam.horizontalCenter
        text: "Test Camera (uniquement local)"
        onClicked: {
            presentation.visible = !presentation.visible
            cam.visible = !cam.visible
        }
    }
}
