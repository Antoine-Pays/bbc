import QtQuick 2.0
import QtQuick.Controls 2.0


Rectangle {
    border.width: 1
    color: "#1F70F2"
    Column{
        anchors.fill:parent
        TextEdit{
            textMargin: 5
            readOnly: true
            anchors.horizontalCenter: parent.horizontalCenter
            text :"Rooms : "
        }

        ComboBox {
            id:room
            anchors.horizontalCenter: parent.horizontalCenter
            model: roomModel

            onActivated: {
                roomModel.setCurrentIndex(currentIndex);
            }

            textRole:"nom"

            Connections {
                target: pannelRoot
                onResetRoomCB:{
                    room.currentIndex = 0;
                }
            }

            Component.onCompleted: {
                roomModel.list = mgr.currentRoomList
                currentIndex = 0

            }
        }
    }
}
