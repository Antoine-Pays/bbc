import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0

Frame {
    property string title
    property string nameField
    property string adresseField
    property string caller
    ColumnLayout{
        TextEdit{
            Layout.alignment: Qt.AlignHCenter
            text:title
        }

        RowLayout{
            TextEdit{
                text:"Nom : "
                readOnly: true
            }
            TextField{
                id:nomF
                text:nameField
            }
        }
        RowLayout{
            TextEdit{
                text:"Adresse : "
                readOnly: true
            }
            TextField{
                id:addF
                text:adresseField
            }
        }
        RowLayout{
            Layout.alignment: Qt.AlignHCenter
            Button{
                text:"Ok"

                onClicked: {
                    switch(caller){
                        case "dialogMod":
                            mgr.setCurrentServer2(nomF.text,addF.text)
                            dialogMod.close()
                            break;
                        case "dialogAdd":
                            mgr.appendServer(nomF.text,addF.text)
                            server.currentIndex = server.count-1
                            serverModel.setCurrentIndex(server.currentIndex);
                            dialogAdd.close()
                            break;
                    }
                }
            }
            Button{
                text:"Cancel"

                onClicked: {
                    switch(caller){
                        case "dialogMod":
                            dialogMod.close()
                            break;
                        case "dialogAdd":
                            dialogAdd.close()
                            break;
                    }
                }
            }
        }
    }
}
