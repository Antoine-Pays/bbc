import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.0

Rectangle {
    border.width: 1
    color: "#1F70F2"
    ColumnLayout{
        anchors.fill:parent
        spacing: 2
        TextEdit{
            textMargin: 5
            readOnly: true
            Layout.alignment: Qt.AlignHCenter
            text :"Serveurs :"

        }

        ComboBox {
            id:server

            Layout.alignment: Qt.AlignHCenter
            model: serverModel

            onActivated: {
                serverModel.setCurrentIndex(currentIndex);
                pannelRoot.resetRoomCB()
            }

            textRole:"nom"

            Component.onCompleted: {
                serverModel.list = mgr.serverList
                currentIndex = 0
            }
        }
        Row{
            Layout.alignment: Qt.AlignHCenter
            width: parent.width
            Rectangle{
                id:recAdd
                width: parent.width/3
                height: width
                color:"transparent"
                radius:2
                Button{
                    background: Image{
                        source: "qrc:/image/plus--v1.png"
                        mipmap: true
                    }
                    anchors.centerIn: parent
                    width: parent.width-5
                    height: width
                    id:buttonAdd

                    onClicked: {
                        ddAdd.nameField = ""
                        ddAdd.adresseField = ""
                        dialogAdd.open()
                    }
                    onPressed: {
                        recAdd.color = "grey"
                    }
                    onReleased: {
                        recAdd.color = "transparent"
                    }
                    MessageDialog {
                        id: dialogAdd
                        title: "Ajouter un serveur"
                        contentItem: DialogDisplay{
                            id:ddAdd
                            title: "Ajouter un serveur"
                            caller: "dialogAdd"
                            nameField: ""
                            adresseField: ""
                        }
                        Component.onCompleted: visible = false
                    }
                }
            }

            Rectangle{
                id:recMod
                width: parent.width/3
                height: width
                color:"transparent"
                radius:2
                Button{
                    background: Image{
                        source: "qrc:/image/edit--v1.png"
                        mipmap: true
                    }
                    anchors.centerIn: parent
                    width: parent.width-5
                    height: width
                    id:buttonMod

                    onClicked: {
                        ddMod.nameField = mgr.currentServer.nomServer
                        ddMod.adresseField = mgr.currentServer.lienServer
                        dialogMod.open()
                    }
                    onPressed: {
                        recMod.color = "grey"
                    }
                    onReleased: {
                        recMod.color = "transparent"
                    }
                    MessageDialog {
                        id: dialogMod
                        title: "Modifier un serveur"

                        contentItem: DialogDisplay{
                            id:ddMod
                            title: "Modifier un serveur : \""+server.currentText+"\""
                            nameField: mgr.currentServer.nomServer
                            adresseField: mgr.currentServer.lienServer
                            caller: "dialogMod"
                        }
                        Component.onCompleted: visible = false
                    }
                }
            }

            Rectangle{
                id:recDel
                width: parent.width/3
                height: width
                color:"transparent"
                radius:2
                Button{
                    background: Image{
                        source: "qrc:/image/delete-sign--v1.png"
                        mipmap: true
                    }
                    anchors.centerIn: parent
                    width: parent.width-5
                    height: width
                    id:buttonDel
                    highlighted: true
                    onClicked: {
                        dialogDel.open()
                    }
                    onPressed: {
                        recDel.color = "grey"
                    }
                    onReleased: {
                        recDel.color = "transparent"
                    }

                    MessageDialog {
                        id: dialogDel
                        title: "Supprimer un serveur"
                        text: "Etes-vous sûr de vouloir supprimer \""+server.currentText+"\" définitivement ?"
                        onAccepted: {
                            mgr.removeServer(server.currentIndex)
                            server.currentIndex != 0 ? server.currentIndex-- : 1
                        }
                        Component.onCompleted: visible = false
                    }
                }
            }


        }
    }


}




