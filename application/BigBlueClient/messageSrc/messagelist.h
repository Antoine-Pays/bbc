#ifndef MESSAGELIST_H
#define MESSAGELIST_H

#include <QObject>
#include "message.h"

class MessageList : public QObject
{
    Q_OBJECT

private:
    QList<Message*> m_listMsg;

public:
    explicit MessageList(std::list<Message*> lc, QObject *parent = nullptr);

    QList<Message*> getListMessage();

    bool setItemAt(int index,const Message &item);

signals:
    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem();
    void removeCompletItem();
};

#endif // MESSAGELIST_H
