#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>
#include <QDateTime>
#include "userSrc/user.h"

class Message : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString content READ content WRITE setContent NOTIFY contentChanged)
    Q_PROPERTY(User* author READ author WRITE setAuthor NOTIFY authorChanged)
    Q_PROPERTY(QDateTime date READ date WRITE setDate NOTIFY dateChanged)

    QString m_content;
    User* m_author;
    QDateTime m_date;

public:
    Message(QString content, User* author, QDateTime date,QObject *parent=nullptr );

    QString content() const;
    User* author() const;
    QDateTime date() const;

signals:
    void preContentChanged();
    void postContentChanged();

    void preAuthorChanged();
    void postAuthorChanged();

    void contentChanged(QString content);

    void authorChanged(User* author);

    void dateChanged(QDateTime date);

public slots:
void setContent(QString content);
void setAuthor(User* author);
void setDate(QDateTime date);
};

#endif // MESSAGE_H
