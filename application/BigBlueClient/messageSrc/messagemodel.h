#ifndef MESSAGEMODEL_H
#define MESSAGEMODEL_H

#include <QAbstractListModel>

class MessageList;

class MessageModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(MessageList *list READ list WRITE setList NOTIFY listChanged)

private:
    MessageList *m_listMsg;


public:
    explicit MessageModel(QObject *parent = nullptr);

    enum{
        ContentRole,
        AuthorNameRole,
        AuthorAvatarRole,
        DateTimeRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int,QByteArray> roleNames() const override;

    MessageList *list() const;

    Q_INVOKABLE bool isSameAuthorThanPrevious(int index);


public slots:
    void setList(MessageList *list) ;


signals:
    void listChanged(MessageList * list);
};

#endif // MESSAGEMODEL_H
