#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "messageSrc/messagelist.h"
#include "roomSrc/roomlist.h"
#include "serverSrc/serverlist.h"
#include "userSrc/userlist.h"

#include "messageSrc/messagemodel.h"
#include "roomSrc/roommodel.h"
#include "serverSrc/servermodel.h"
#include "userSrc/usermodel.h"



#include "joliesyntax.h"
#include "manager.h"





int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    //QQuickStyle::setStyle("Material");
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    ServerModel serverModel{};
    qmlRegisterUncreatableType<ServerModel>("ServerModel",1,0,"ServerModel", QStringLiteral("ServerModel should not be created in QML"));
    engine.rootContext()->setContextProperty(QStringLiteral("serverModel"),&serverModel);
    qmlRegisterUncreatableType<ServerList>("ServerList",1,0,"ServerList",QStringLiteral("ServerList should not be created in QML"));

    RoomModel roomModel{};
    qmlRegisterUncreatableType<RoomModel>("RoomModel",1,0,"RoomModel", QStringLiteral("RoomModel should not be created in QML"));
    engine.rootContext()->setContextProperty(QStringLiteral("roomModel"),&roomModel);
    qmlRegisterUncreatableType<RoomList>("RoomList",1,0,"RoomList",QStringLiteral("RoomList should not be created in QML"));

    MessageModel messageModel{};
    qmlRegisterUncreatableType<MessageModel>("MessageModel",1,0,"MessageModel", QStringLiteral("MessageModel should not be created in QML"));
    engine.rootContext()->setContextProperty(QStringLiteral("messageModel"),&messageModel);
    qmlRegisterUncreatableType<MessageList>("MessageList",1,0,"MessageList",QStringLiteral("MessageList should not be created in QML"));

    UserModel userModel{};
    qmlRegisterUncreatableType<UserModel>("UserModel",1,0,"UserModel", QStringLiteral("UserModel should not be created in QML"));
    engine.rootContext()->setContextProperty(QStringLiteral("userModel"),&userModel);
    qmlRegisterUncreatableType<UserList>("UserList",1,0,"UserList",QStringLiteral("UserList should not be created in QML"));

    Manager mgr{};
    qmlRegisterUncreatableType<Manager>("Manager", 1,0, "Manager", QStringLiteral("Manager should not be created in QML"));
    engine.rootContext()->setContextProperty(QStringLiteral("mgr"),&mgr);



    QObject::connect(&serverModel,&ServerModel::updateRoomModel,&roomModel,&RoomModel::setList);
    QObject::connect(&serverModel,&ServerModel::updateRoomModel,&mgr,&Manager::setCurrentRoomList);

    QObject::connect(&roomModel,&RoomModel::updateMessageModel,&messageModel,&MessageModel::setList);
    QObject::connect(&roomModel,&RoomModel::updateMessageModel,&mgr,&Manager::setCurrentMessageList);

    QObject::connect(&roomModel,&RoomModel::updateUserModel, &userModel,&UserModel::setList);
    QObject::connect(&roomModel,&RoomModel::updateUserModel, &mgr,&Manager::setCurrentUserList);

    QObject::connect(&serverModel,&ServerModel::updateCurrentServer,&mgr,&Manager::setCurrentServer);
    QObject::connect(&roomModel,&RoomModel::updateCurrentRoom,&mgr,&Manager::setCurrentRoom);


    qmlRegisterType<JolieSyntaxe>("joliesyntaxe",1,0,"JolieSyntaxe");


    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);



    return app.exec();
}
