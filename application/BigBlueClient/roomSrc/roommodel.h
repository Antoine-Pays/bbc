#ifndef ROOMMODEL_H
#define ROOMMODEL_H

#include <QAbstractListModel>

#include "messageSrc/messagelist.h"

#include "userSrc/userlist.h"

#include "room.h"

class RoomList;

class RoomModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(RoomList *list READ list WRITE setList NOTIFY listChanged)
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY updateMessageModel)

private:
    RoomList *m_listRoom;

    int m_currentIndex;

public:
    explicit RoomModel(QObject *parent = nullptr);

    enum{
        NomRole,
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int,QByteArray> roleNames() const override;

    RoomList *list() const;

    int currentIndex() const;

public slots:
    void setList(RoomList *list) ;


    void setCurrentIndex(int currentIndex);

signals:
    void listChanged(RoomList * list);
    void updateMessageModel(MessageList* messageList);
    void updateUserModel(UserList* userList);
    void updateCurrentRoom(Room *);
};

#endif // ROOMMODEL_H
