#ifndef ROOMLIST_H
#define ROOMLIST_H

#include "room.h"

#include <QObject>

class RoomList : public QObject
{
    Q_OBJECT

private:
    QList<Room*> m_listRoom;

public:
    explicit RoomList(std::list<Room*> lc, QObject *parent = nullptr);

    QList<Room*> getListRoom();

    bool setItemAt(int index,const Room &item);

signals:
    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem();
    void removeCompletItem();


};

#endif // ROOMLIST_H
