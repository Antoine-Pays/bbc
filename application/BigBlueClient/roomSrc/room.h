#ifndef ROOM_H
#define ROOM_H

#include <QObject>
#include <QUrl>
#include "userSrc/userlist.h"
#include "../messageSrc/messagelist.h"

class Room : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nomRoom READ nomRoom WRITE setNomRoom NOTIFY nomRoomChanged)
    Q_PROPERTY(MessageList *listMsg READ listMsg WRITE setListMsg NOTIFY listMsgChanged)
    Q_PROPERTY(UserList* listUser READ listUser WRITE setListUser NOTIFY listUserChanged)

    QString m_nomRoom;
    MessageList *m_listMsg;

    UserList* m_listUser;

public:
    Room(QString nomRoom, MessageList* listMsg, UserList* listUser, QObject *parent=nullptr );

    QString nomRoom() const;
    MessageList * listMsg() const;
    UserList* listUser() const;

signals:
    void preNomServChanged();
    void postNomServChanged();

    void preSourceImgChanged();
    void postSourceImgChanged();

    void nomRoomChanged(QString nomRoom);
    void listMsgChanged(MessageList * listMsg);
    void listUserChanged(UserList* listUser);

public slots :

    void setNomRoom(QString nomRoom);
    void setListMsg(MessageList * listMsg);
    void setListUser(UserList* listUser);
};

#endif // ROOM_H
