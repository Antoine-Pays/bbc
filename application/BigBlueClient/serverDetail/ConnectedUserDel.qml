import QtQuick 2.0
import QtQuick.Controls 2.0

ItemDelegate{
    implicitWidth: parent.width
    implicitHeight: 45

    Rectangle{
        id:baseRec
        anchors.fill:parent
        Rectangle{
            id:imgRec
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height
            width: parent.height
            color:"transparent"

            Image {
                id: avatarImg
                anchors.fill:parent
                source: model.avatar

            }
        }
        Rectangle{
            id:textRec
            anchors.verticalCenter: parent.verticalCenter
            height: 50
            width: parent.width-imgRec.width
            anchors.left: imgRec.right
            radius: 20

            Text {
                anchors.centerIn: parent
                text: model.nom
            }
        }


    }

}
