import QtQuick 2.1
import QtQuick.Controls 2.0
import joliesyntaxe 1.0

Item{
    Rectangle{

        id:baseRec
        anchors.margins: 10
        anchors.fill:parent

        Rectangle{
            id: contRec
            width: parent.width*0.8
            height: parent.height
            border.color: "grey"
            border.width: 2
            radius: 2
            Flickable {
                 id: flick
                 anchors.fill:parent
                 contentWidth: edit.paintedWidth
                 contentHeight: edit.paintedHeight
                 clip: true

                 function ensureVisible(r)
                 {
                     if (contentX >= r.x)
                         contentX = r.x;
                     else if (contentX+width <= r.x+r.width)
                         contentX = r.x+r.width-width;
                     if (contentY >= r.y)
                         contentY = r.y;
                     else if (contentY+height <= r.y+r.height)
                         contentY = r.y+r.height-height;
                 }
                 JolieSyntaxe{
                     id:jl
                 }
                 TextEdit {
                     id: edit
                     width: flick.width
                     height: flick.height
                     wrapMode: TextEdit.Wrap
                     textMargin: 5
                     selectByMouse: true
                     onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                     onFocusChanged: {
                         if(focus){
                             placeholder.visible = false
                             contRec.border.color = "#1F70F2"
                         }
                         else{
                             if (!edit.text)
                                placeholder.visible = true
                             contRec.border.color = "grey"
                         }
                     }
                     Component.onCompleted: {
                         jl.setDocument(edit.textDocument)
                     }

                     TextEdit {
                         id:placeholder
                         text: "Enter text here..."
                         color: "#888"
                         readOnly: true
                         textMargin: 5
                     }
                 }
             }
        }

        Rectangle{
            id: authRec

            anchors.left: contRec.right
            width: parent.width*0.2
            height: parent.height
            Image{
                source: "qrc:/image/envoyer.png"
                anchors.fill:parent
                fillMode: Image.PreserveAspectFit
                anchors.margins: 12
                anchors.centerIn: parent
            }

        }

    }


}



