import QtQuick 2.0
import QtQuick.Controls 2.0


Frame {
    padding: 10
    ListView{
        anchors.fill:parent
        clip: true
        spacing: 15
        model: userModel

        delegate: ConnectedUserDel {}

        ScrollBar.vertical: ScrollBar {}

    }
}


