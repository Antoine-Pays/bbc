import QtQuick 2.0
import QtQuick.Controls 2.0

//import Message 1.0

Item {
    property int tfHeight: 100
    Frame {
        anchors.fill:parent
        padding: 1
        ListView{
            implicitWidth: parent.width
            implicitHeight: parent.height-tfHeight
            clip: true
            model: messageModel
            delegate: ChatDel {}

            ScrollBar.vertical: ScrollBar {}
        }
    }
    TField{
        implicitHeight: tfHeight
        implicitWidth: parent.width
        anchors.bottom:parent.bottom
    }
}
