import QtQuick 2.0
import QtQuick.Controls 2.0
import joliesyntaxe 1.0

ItemDelegate{
    property int textWidth: 300

    implicitWidth: parent.width
    implicitHeight: baseRec.height

    Rectangle{
        id:baseRec
        width:parent.width
        height:contRec.height
        color: index % 2 == 0 ? "#85c1e9" : "#aed6f1"
        Rectangle{
            id: authRec

            width: parent.width-textWidth
            height: parent.height
            color:"transparent"
            Image {
                visible: messageModel.isSameAuthorThanPrevious(index)
                anchors.top: parent.top
                anchors.topMargin: 5
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width-10
                fillMode: Image.PreserveAspectFit
                source: model.authorAvatar

            }

        }

        Rectangle{
            id: contRec
            width: textWidth
            height: msg.height+ (author.visible ? author.height :0)
            anchors.left: authRec.right
            color:"transparent"
            Column{
                anchors.fill: parent

                Row{
                    width: parent.width
                    visible :messageModel.isSameAuthorThanPrevious(index)
                    TextEdit{
                        id:author
                        text: model.authorName
                        font.bold: true
                        font.pixelSize: 16
                        textMargin: 6
                        readOnly: true
                    }
                    TextEdit{
                        id:date
                        anchors.verticalCenter: author.verticalCenter
                        text: model.dateTime
                        font.italic: true
                        font.pixelSize: 10
                        color:"dimgrey"
                        textMargin: 6
                        readOnly: true
                    }
                }
                JolieSyntaxe{
                    id:jl
                }
                TextEdit{
                    id:msg
                    width:parent.width
                    readOnly: true
                    wrapMode: TextEdit.Wrap
                    text: model.content
                    textMargin: 7
                    Component.onCompleted: {
                        jl.setDocument(msg.textDocument)
                    }
                }
            }


        }


    }



}
