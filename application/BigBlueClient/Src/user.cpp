#include "userSrc/user.h"

User::User(QUrl avatarUser, QString nomUser, QObject *parent) : QObject (parent)
{
    this->m_avatarUser = avatarUser;
    this->m_nomUser = nomUser;
}

QString User::nomUser() const
{
    return m_nomUser;
}

QUrl User::avatarUser() const
{
    return m_avatarUser;
}

void User::setNomUser(QString nomUser)
{
    if (m_nomUser == nomUser)
        return;

    m_nomUser = nomUser;
    emit nomUserChanged(m_nomUser);
}

void User::setAvatarUser(QUrl avatarUser)
{
    if (m_avatarUser == avatarUser)
        return;

    m_avatarUser = avatarUser;
    emit avatarUserChanged(m_avatarUser);
}
