#include "manager.h"
#include <QDebug>



Manager::Manager(QObject *parent) : QObject(parent)
{
    m_serverList = new ServerList(m_dataGW.getStub());
    m_currentServer = m_serverList->getListServer().first();

    m_currentRoomList = m_currentServer->listRoom();
    m_currentRoom = m_currentRoomList->getListRoom().first();

    m_currentMessageList = m_currentRoom->listMsg();
    m_currentUserList = m_currentRoom->listUser();
}

ServerList *Manager::serverList() const
{
    return m_serverList;
}

Server *Manager::currentServer() const
{
    return m_currentServer;
}

Room *Manager::currentRoom() const
{
    return m_currentRoom;
}

MessageList *Manager::currentMessageList() const
{
    return m_currentMessageList;
}

RoomList *Manager::currentRoomList() const
{
    return m_currentRoomList;
}

UserList *Manager::currentUserList() const
{
    return m_currentUserList;
}



void Manager::setServerList(ServerList *listServer)
{
    if (m_serverList == listServer)
        return;

    m_serverList = listServer;
    emit serverListChanged(m_serverList);
}

void Manager::setCurrentServer(Server *currentServer)
{
    if (m_currentServer == currentServer)
        return;
    qDebug() << "SET CURRENT SERVER >> " << currentServer->nomServer();
    m_currentServer = currentServer;
    m_currentRoomList = m_currentServer->listRoom();
    m_currentRoom = m_currentRoomList->getListRoom().first();
    m_currentMessageList = m_currentRoom->listMsg();
}

void Manager::setCurrentServer2(QString nom, QString lien){
    m_currentServer->setNomServer(nom);
    m_currentServer->setLienServer(lien);
}

void Manager::setCurrentRoom(Room *currentRoom)
{
    if (m_currentRoom == currentRoom)
        return;

    m_currentRoom = currentRoom;
    emit currentRoomChanged(m_currentRoom);
}

void Manager::setCurrentRoomList(RoomList *currentRoomList)
{
    if (m_currentRoomList == currentRoomList)
        return;

    m_currentRoomList = currentRoomList;
    emit currentRoomListChanged(m_currentRoomList);
}

void Manager::setCurrentUserList(UserList *listUser)
{
    if (m_currentUserList == listUser)
        return;

    m_currentUserList = listUser;
    emit currentUserListChanged(m_currentUserList);
}

void Manager::setCurrentMessageList(MessageList *currentMessageList)
{
    if (m_currentMessageList == currentMessageList)
        return;

    m_currentMessageList = currentMessageList;
    emit currentMessageListChanged(m_currentMessageList);
}
