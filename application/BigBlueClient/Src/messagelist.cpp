#include "messageSrc/messagelist.h"


MessageList::MessageList(std::list<Message*> lc, QObject *parent) : QObject(parent)
{
    for(Message* m : lc){
        m_listMsg.append(m);
    }
}

QList<Message*> MessageList::getListMessage() {
    return m_listMsg;
}

bool MessageList::setItemAt(int index, const Message &item)
{
    if(index < 0 || index >= m_listMsg.size())
        return false;
    const Message* oldItem = m_listMsg.at(index);
    if(item.content() == oldItem->content()
            && item.author() == oldItem->author()
            && item.date() == oldItem->date())
        return false;
    m_listMsg[index]->setContent(QString(item.content()));
    m_listMsg[index]->setAuthor(new User(item.author()->avatarUser(), item.author()->nomUser()));
    m_listMsg[index]->setDate(QDateTime());
    return true;
}

void MessageList::appendItem()
{
    emit preItemAppended();
    Message* item = new Message(QString(), new User(QUrl(), QString()), QDateTime());
    m_listMsg.append(item);

    emit postItemAppended();
}

void MessageList::removeCompletItem()
{
    /*for(int i =0; i< listMsg.size(); ){
        if(listMsg.at(i)->content()){
            emit preItemAppended();

            listMsg.removeAt(i);

            emit postItemAppended();
        }
        else{
            ++i;
        }
    }*/
}
