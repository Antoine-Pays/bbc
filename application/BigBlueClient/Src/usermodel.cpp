#include "userSrc/usermodel.h"
#include "userSrc/userlist.h"

#include <iostream>

UserModel::UserModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_listUser(nullptr)
{
}

int UserModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !m_listUser)
        return 0;

    return m_listUser->getListUser().size();
}

QVariant UserModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !m_listUser)
        return QVariant();
    const User *item = m_listUser->getListUser().at(index.row());
    switch (role) {
        case NomRole :
            return QVariant(item->nomUser());
        case AvatarRole :
            return QVariant(item->avatarUser());
    }
    return QVariant();
}


bool UserModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!m_listUser)
        return false;
    User *item = m_listUser->getListUser()[index.row()];
    switch (role) {
        case NomRole :
            item->setNomUser(value.toString());
            break;
        case AvatarRole :
            item->setAvatarUser(value.toUrl());
            break;
    }

    if (m_listUser->setItemAt(index.row(),*item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags UserModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> UserModel::roleNames() const
{
     QHash<int, QByteArray> names;
     names[NomRole]= "nom";
     names[AvatarRole]="avatar";
     return names;
}

UserList *UserModel::list() const
{
    return m_listUser;
}

void UserModel::setList(UserList *list)
{
    beginResetModel();

    if(m_listUser)
        m_listUser->disconnect(this);

    m_listUser = list;

    if(m_listUser){
        connect(m_listUser, &UserList::preItemAppended, this,[=](){
            const int index = m_listUser->getListUser().size();
            beginInsertRows(QModelIndex(),index, index);
        });
        connect(m_listUser, &UserList::postItemAppended, this,[=](){
            endInsertRows();
        });
        connect(m_listUser, &UserList::preItemRemoved, this,[=](int index){
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(m_listUser, &UserList::postItemRemoved, this,[=](){
            endRemoveRows();
        });
    }
    endResetModel();
}
