#include "datagateway.h"

#include "serverSrc/server.h"

DataGateway::DataGateway()
{

}

std::list<Server *> DataGateway::getStub()
{
    std::list<Message*>m{
                new Message("YEYEYEYEYE@orange.com Le principe de la suralimentation des moteurs à combustion et explosion a été proposé dès les premiers "
                                              "développements de ces moteurs. Louis Renault dépose en 1902 un brevet sur le principe de suralimentation "
                                              "par ventilateur ou compresseur, qu'il utilise en compétition, mais qui n'est pas encore défini comme un "
                                              "turbocompresseur2. ",new User(QUrl("qrc:/image/logo.png"),"tom"), QDateTime(QDateTime::currentDateTime())),
                new Message("Le 13 novembre 1905, le brevet du principe du turbocompresseur est concédé à l’ingénieur suisse Alfred "
                                               "Büchi par la Deutsches Reichspatent (DRP) et le 16 novembre 1905, un autre pour son application au "
                                               "moteur à explosion3. Il s'agissait d'un compresseur centrifuge entraîné cette fois par les gaz "
                                               "d'échappement. Une des premières applications a été l'adaptation par l’ingénieur Auguste Rateau du "
                                               "turbocompresseur sur le moteur Renault 12 Fe, un V12 de 320 ch équipant l'avion de reconnaissance "
                                               "Breguet XIV A2 pendant la Première Guerre mondiale4.",new User(QUrl("qrc:/image/logo.png"),"tom"), QDateTime(QDateTime::currentDateTime())),
                new Message("On a assisté à un développement important du turbocompresseur lors de la Seconde Guerre mondiale où "
                                               "le « turbo » a été indispensable pour permettre à des avions dotés de moteurs à piston de voler à haute "
                                               "altitude. En effet, l'air devenant plus rare à partir de 3 000 à 4 000 mètres, un moteur atmosphérique "
                                               "perd de la puissance et « cale » du fait du peu d’oxygène contenu dans l'air. ",new User(QUrl("qrc:/image/logo.png"),"denis"), QDateTime(QDateTime::currentDateTime())),
                new Message("La technique de suralimentation est très souvent appliquée aux moteurs des automobiles de course. En "
                                               "Formule 1 par exemple, elle a révolutionné la motorisation à partir de 1977 avec Renault et remporté de "
                                               "nombreux succèsb avant d'être interdite en 1989. La saison 2014 du championnat voit son retour dans la "
                                               "discipline, avec un moteur V6 de 1 600 cm3 turbocompressé5.",new User(QUrl("qrc:/image/logo.png"),"antoine"), QDateTime(QDateTime::currentDateTime())),
                new Message("Outre le phénomène de mode lancé principalement par Renault dans les années 1980[réf. nécessaire] "
                                               "(R5 Turbo, Alpine turbo, R5 GT Turbo, etc.), une autre apparition prouvera que le turbo ne sert pas "
                                               "qu'à « gonfler des petites voitures légères à moindre coût ». En effet, l'arrivée de la tonitruante "
                                               "Porsche 911 Turbo 3,0 L (type 930)[non neutre] lancera à son tour toute une génération de passionnés de "
                                               "voitures de grand tourisme (GT), cette dernière étant quasiment sans rivale au moment de sa sortie, "
                                               "grâce à ses performances impressionnantes pour l'époque[réf. nécessaire]. ",new User(QUrl("qrc:/image/logo.png"),"alexis"), QDateTime(QDateTime::currentDateTime()))
    };
    std::list<Message*>mbis{new Message("content 1 bis",new User(QUrl("qrc:/image/logo.png"),"timothe"), QDateTime(QDateTime::currentDateTime()))};
    std::list<Message*>m2{new Message("content 2",new User(QUrl("qrc:/image/logo.png"),"timothe"), QDateTime(QDateTime::currentDateTime()))};
    std::list<Message*>m3{new Message("content 3",new User(QUrl("qrc:/image/logo.png"),"antoine"), QDateTime(QDateTime::currentDateTime()))};
    std::list<Message*>m4{new Message("content 4",new User(QUrl("qrc:/image/logo.png"),"alexis"), QDateTime(QDateTime::currentDateTime()))};
    std::list<Message*>m5{new Message("content 5",new User(QUrl("qrc:/image/logo.png"),"tom"), QDateTime(QDateTime::currentDateTime()))};

    std::list<User*>u{new User(QUrl("qrc:/image/logo.png"),"timothe"),
                     new User(QUrl("qrc:/image/logo.png"),"antoine")};
    std::list<User*>ubis{new User(QUrl("qrc:/image/logo.png"),"alexei")};
    std::list<User*>u2{new User(QUrl("qrc:/image/logo.png"),"alexei2")};
    std::list<User*>u3{new User(QUrl("qrc:/image/logo.png"),"alexei3")};
    std::list<User*>u4{new User(QUrl("qrc:/image/logo.png"),"alexei4")};
    std::list<User*>u5{new User(QUrl("qrc:/image/logo.png"),"alexei5")};

    std::list<Room*>r{new Room("nom1", new MessageList(m), new UserList(u)),
                     new Room("nom1bis", new MessageList(mbis), new UserList(ubis))};
    std::list<Room*>r2{new Room("nom2", new MessageList(m2), new UserList(u2))};
    std::list<Room*>r3{new Room("nom3", new MessageList(m3), new UserList(u3))};
    std::list<Room*>r4{new Room("nom4", new MessageList(m4), new UserList(u4))};
    std::list<Room*>r5{new Room("nom5", new MessageList(m5), new UserList(u5))};
    std::list<Server*>s{new Server("lien1","server test1", new RoomList(r)),
                       new Server("lien2","server test2", new RoomList(r2)),
                       new Server("lien3","server test3", new RoomList(r3)),
                       new Server("lien4","server test4", new RoomList(r4)),
                        new Server("lien5","server test5", new RoomList(r5))};
    return s;
}
