#include "messageSrc/messagemodel.h"

#include "messageSrc/messagelist.h"

MessageModel::MessageModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_listMsg(nullptr)
{
}

int MessageModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !m_listMsg)
        return 0;

    return m_listMsg->getListMessage().size();
}

QVariant MessageModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !m_listMsg)
        return QVariant();
    const Message *item = m_listMsg->getListMessage().at(index.row());
    switch (role) {
        case ContentRole :
            return QVariant(item->content());
        case AuthorNameRole :
            return QVariant(item->author()->nomUser());
        case AuthorAvatarRole:
            return QVariant(item->author()->avatarUser());
        case DateTimeRole:
            return QVariant(item->date().toString("hh:mm"));
    }
    return QVariant();
}


bool MessageModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!m_listMsg)
        return false;
    Message *item = m_listMsg->getListMessage()[index.row()];
    switch (role) {
        case ContentRole :
            item->setContent(value.toString());
            break;
        case AuthorNameRole :
            item->author()->setNomUser(value.toString());
            break;
        case AuthorAvatarRole :
            item->author()->setAvatarUser(value.toString());
            break;
        case DateTimeRole :
            item->setDate(value.toDateTime());
            break;

    }

    if (m_listMsg->setItemAt(index.row(),*item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags MessageModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> MessageModel::roleNames() const
{
     QHash<int, QByteArray> names;
     names[ContentRole]= "content";
     names[AuthorNameRole]= "authorName";
     names[AuthorAvatarRole]= "authorAvatar";
     names[DateTimeRole] = "dateTime";
     return names;
}

MessageList *MessageModel::list() const
{
    return m_listMsg;
}

bool MessageModel::isSameAuthorThanPrevious(int index){
    if (index <= 0) return true;
    if (m_listMsg->getListMessage().at(index-1)->author()->nomUser()
            == m_listMsg->getListMessage().at(index)->author()->nomUser())
        return  false;
    else {
        return true;
    }
}

void MessageModel::setList(MessageList *list)
{
    beginResetModel();

    if(m_listMsg)
        m_listMsg->disconnect(this);

    m_listMsg = list;

    if(m_listMsg){
        connect(m_listMsg, &MessageList::preItemAppended, this,[=](){
            const int index = m_listMsg->getListMessage().size();
            beginInsertRows(QModelIndex(),index, index);
        });
        connect(m_listMsg, &MessageList::postItemAppended, this,[=](){
            endInsertRows();
        });
        connect(m_listMsg, &MessageList::preItemRemoved, this,[=](int index){
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(m_listMsg, &MessageList::postItemRemoved, this,[=](){
            endRemoveRows();
        });
    }
    endResetModel();
}
