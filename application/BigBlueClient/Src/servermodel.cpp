
#include <QUrl>
#include "serverSrc/servermodel.h"
#include "serverSrc/serverlist.h"

#include <QDebug>

ServerModel::ServerModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_listServer(nullptr)
{
}

int ServerModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !m_listServer)
        return 0;

    return m_listServer->getListServer().size();
}

QVariant ServerModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !m_listServer)
        return QVariant();
    const Server *item = m_listServer->getListServer().at(index.row());
    switch (role) {
        case LienRole :
            return QVariant(item->lienServer());
        case NomRole :
            return QVariant(item->nomServer());
    }
    return QVariant();
}


bool ServerModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!m_listServer)
        return false;
    Server *item = m_listServer->getListServer()[index.row()];
    switch (role) {
        case LienRole :
            item->setLienServer(value.toString());
            break;
        case NomRole :
            item->setNomServer(value.toString());
            break;
    }

    if (m_listServer->setItemAt(index.row(),*item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ServerModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> ServerModel::roleNames() const
{
     QHash<int, QByteArray> names;
     names[LienRole]= "lien";
     names[NomRole]= "nom";
     return names;
}

ServerList *ServerModel::list() const
{
    return m_listServer;
}

int ServerModel::currentIndex() const
{
    return m_currentIndex;
}

void ServerModel::setList(ServerList *list)
{
    beginResetModel();

    if(m_listServer)
        m_listServer->disconnect(this);

    m_listServer = list;

    if(m_listServer){
        connect(m_listServer, &ServerList::preItemAppended, this,[=](){
            const int index = m_listServer->getListServer().size();
            beginInsertRows(QModelIndex(),index, index);
        });
        connect(m_listServer, &ServerList::postItemAppended, this,[=](){
            endInsertRows();
        });
        connect(m_listServer, &ServerList::preItemRemoved, this,[=](int index){
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(m_listServer, &ServerList::postItemRemoved, this,[=](){
            endRemoveRows();
        });
        connect(m_listServer, &ServerList::itemChanged,this,[=](int row){
            qDebug() << "emit dataChanged à l'index : " <<row;
            emit dataChanged(index(row),index(row));
            emit layoutChanged();
        });
    }
    endResetModel();
    setCurrentIndex(0);

}

void ServerModel::setCurrentIndex(int currentIndex)
{

    if (m_currentIndex == currentIndex)
        return;
    m_currentIndex = currentIndex;
    Server* s= m_listServer->getListServer().at(m_currentIndex);
    qDebug() << "SET CURRENT INDEX";
    emit updateCurrentServer(s);
    emit updateRoomModel(s->listRoom());
}




