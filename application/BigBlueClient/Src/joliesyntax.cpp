#include "joliesyntax.h"

#include <iostream>

//--------------------------------constructeur------------------------------------------------

JolieSyntaxe::JolieSyntaxe(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    //detection des mails
    emailFormat.setFontItalic(true);
    emailFormat.setForeground(Qt::blue);
    rule.pattern = QRegularExpression(QStringLiteral("\\b[A-Za-z0-9_]+@[A-Za-z0-9_.]+\\.[A-Za-z.]+\\b"));
    rule.format = emailFormat;
    highlightingRules.append(rule);

    //detection des liens https
    httpFormat.setFontUnderline(true);
    httpFormat.setForeground(Qt::darkCyan);
    rule.pattern = QRegularExpression(QStringLiteral("\\bhttps?://[^\\s]+\\b"));
    rule.format = httpFormat;
    highlightingRules.append(rule);

}

//-------------------------------Fin-constructeur------------------------------------------------

void JolieSyntaxe::highlightBlock(const QString &text)
{
    for (const HighlightingRule &rule : qAsConst(highlightingRules)) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext()) {
                QRegularExpressionMatch match = matchIterator.next();
                setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }
}


void JolieSyntaxe::setDocument(QQuickTextDocument *pDoc)
{
    QSyntaxHighlighter::setDocument(pDoc->textDocument());
}




