#include "serverSrc/serverlist.h"

#include <QDebug>

ServerList::ServerList(std::list<Server*> lc, QObject *parent) : QObject(parent)
{
    for(Server* s : lc){
        append(s);
    }

}

QList<Server*> ServerList::getListServer() {
    return m_listServ;
}

bool ServerList::setItemAt(int index, const Server &item)
{
    if(index < 0 || index >= m_listServ.size())
        return false;
    const Server* oldItem = m_listServ.at(index);
    if(item.lienServer() == oldItem->lienServer()
            && item.nomServer() == oldItem->nomServer())
        return false;
    m_listServ[index]->setNomServer(QString(item.nomServer()));
    m_listServ[index]->setLienServer(QString(item.lienServer()));
    return true;
}

void ServerList::appendItem()
{
    emit preItemAppended();
    Server* item = new Server(QString(), nullptr, nullptr);
    m_listServ.append(item);

    emit postItemAppended();
}

void ServerList::append(Server *item){
    emit preItemAppended();
    m_listServ.append(item);
    int i = m_listServ.length();
    connect(item, &Server::nomServerChanged,this,[=]{qDebug() << "Insertion+link à l'index : " <<i; emit itemChanged(i);});
    connect(item, &Server::lienServerChanged,this,[=]{emit itemChanged(i);});
    emit postItemAppended();
}

void ServerList::removeCompletItem()
{
    /*for(int i =0; i< mItems.size(); ){
        if(mItems.at(i).source){
            emit preItemAppended();

            mItems.removeAt(i);

            emit postItemAppended();
        }
        else{
            ++i;
        }
    }*/
}

void ServerList::removeItem(int index){
    emit preItemRemoved(index);

    m_listServ.removeAt(index);

    emit postItemRemoved();
}
