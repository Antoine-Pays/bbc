#include "userSrc/userlist.h"

UserList::UserList(std::list<User*> lc, QObject *parent) : QObject(parent)
{
    for(User* u : lc){
        m_listUser.append(u);
    }
}

QList<User*> UserList::getListUser() {
    return m_listUser;
}

bool UserList::setItemAt(int index, const User &item)
{
    if(index < 0 || index >= m_listUser.size())
        return false;
    const User* oldItem = m_listUser.at(index);
    if(item.nomUser() == oldItem->nomUser())
        return false;
    m_listUser[index]->setNomUser(QString(item.nomUser()));
    m_listUser[index]->setAvatarUser(QUrl(item.avatarUser()));
    return true;
}

void UserList::appendItem()
{
    emit preItemAppended();
    User* item = new User(QUrl(),QString());
    m_listUser.append(item);

    emit postItemAppended();
}

void UserList::removeCompletItem()
{
    /*for(int i =0; i< mItems.size(); ){
        if(mItems.at(i).source){
            emit preItemAppended();

            mItems.removeAt(i);

            emit postItemAppended();
        }
        else{
            ++i;
        }
    }*/
}
