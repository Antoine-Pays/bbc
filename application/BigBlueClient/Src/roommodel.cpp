#include "roomSrc/roommodel.h"
#include "roomSrc/roomlist.h"

#include <iostream>

RoomModel::RoomModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_listRoom(nullptr)
{
}

int RoomModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !m_listRoom)
        return 0;

    return m_listRoom->getListRoom().size();
}

QVariant RoomModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !m_listRoom)
        return QVariant();
    const Room *item = m_listRoom->getListRoom().at(index.row());
    switch (role) {
        case NomRole :
            return QVariant(item->nomRoom());
    }
    return QVariant();
}


bool RoomModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!m_listRoom)
        return false;
    Room *item = m_listRoom->getListRoom()[index.row()];
    switch (role) {
        case NomRole :
            item->setNomRoom(value.toString());
            break;
    }

    if (m_listRoom->setItemAt(index.row(),*item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags RoomModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> RoomModel::roleNames() const
{
     QHash<int, QByteArray> names;
     names[NomRole]= "nom";
     return names;
}

RoomList *RoomModel::list() const
{
    return m_listRoom;
}

int RoomModel::currentIndex() const
{
    return m_currentIndex;
}

void RoomModel::setList(RoomList *list)
{
    beginResetModel();

    if(m_listRoom)
        m_listRoom->disconnect(this);

    m_listRoom = list;

    if(m_listRoom){
        connect(m_listRoom, &RoomList::preItemAppended, this,[=](){
            const int index = m_listRoom->getListRoom().size();
            beginInsertRows(QModelIndex(),index, index);
        });
        connect(m_listRoom, &RoomList::postItemAppended, this,[=](){
            endInsertRows();
        });
        connect(m_listRoom, &RoomList::preItemRemoved, this,[=](int index){
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(m_listRoom, &RoomList::postItemRemoved, this,[=](){
            endRemoveRows();
        });
    }
    endResetModel();
    setCurrentIndex(0);
}

void RoomModel::setCurrentIndex(int currentIndex)
{
    m_currentIndex = currentIndex;
    Room* r = m_listRoom->getListRoom().at(m_currentIndex);
    emit updateMessageModel(r->listMsg());
    emit updateUserModel(r->listUser());
    emit updateCurrentRoom(r);
}
