#include "roomSrc/room.h"

Room::Room(QString nomServeur, MessageList * listMsg, UserList* listUser, QObject *parent) : QObject(parent)
{
    this->m_nomRoom=nomServeur;
    this->m_listMsg=listMsg;
    this->m_listUser=listUser;
}

QString Room::nomRoom() const
{
    return m_nomRoom;
}


MessageList *Room::listMsg() const
{
    return m_listMsg;
}

UserList *Room::listUser() const
{
    return m_listUser;
}

void Room::setNomRoom(QString nomRoom)
{
    if (m_nomRoom == nomRoom)
        return;

    m_nomRoom = nomRoom;
    emit nomRoomChanged(m_nomRoom);
}


void Room::setListMsg(MessageList *list)
{
    if (m_listMsg == list)
        return;
    m_listMsg = list;
    emit listMsgChanged(m_listMsg);
}

void Room::setListUser(UserList *listUser)
{
    if (m_listUser == listUser)
        return;

    m_listUser = listUser;
    emit listUserChanged(m_listUser);
}
