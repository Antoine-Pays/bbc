#include "serverSrc/server.h"


Server::Server(QString lienServer, QString nomServeur, RoomList * listRoom,QObject *parent) : QObject(parent)
{
    this->m_lienServer=lienServer;
    this->m_nomServer=nomServeur;
    this->m_listRoom=listRoom;
}

QString Server::nomServer() const
{
    return m_nomServer;
}

RoomList *Server::listRoom() const
{
    return m_listRoom;
}

QString Server::lienServer() const
{
    return m_lienServer;
}


void Server::setNomServer(QString nomServer)
{
    if (m_nomServer == nomServer)
        return;

    m_nomServer = nomServer;
    emit nomServerChanged(m_nomServer);
}



void Server::setListRoom(RoomList *listRoom)
{
    if (m_listRoom == listRoom)
        return;

    m_listRoom = listRoom;
    emit listRoomChanged(m_listRoom);
}

void Server::setLienServer(QString lienServer)
{
    if (m_lienServer == lienServer)
        return;

    m_lienServer = lienServer;
    emit lienServerChanged(m_lienServer);
}
