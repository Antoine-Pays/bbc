#include "roomSrc/roomlist.h"


RoomList::RoomList(std::list<Room*> lc, QObject *parent) : QObject(parent)
{
    for(Room* r : lc){
        m_listRoom.append(r);
    }
}

QList<Room*> RoomList::getListRoom() {
    return m_listRoom;
}

bool RoomList::setItemAt(int index, const Room &item)
{
    if(index < 0 || index >= m_listRoom.size())
        return false;
    const Room* oldItem = m_listRoom.at(index);
    if(item.nomRoom() == oldItem->nomRoom())
        return false;
    m_listRoom[index]->setNomRoom(item.nomRoom());
    m_listRoom[index]->setListMsg(item.listMsg());
    m_listRoom[index]->setListUser(item.listUser());
    return true;
}

void RoomList::appendItem()
{
    emit preItemAppended();
    Room* item = new Room(nullptr, nullptr, nullptr);
    m_listRoom.append(item);

    emit postItemAppended();
}

void RoomList::removeCompletItem()
{
    /*for(int i =0; i< mItems.size(); ){
        if(mItems.at(i).source){
            emit preItemAppended();

            mItems.removeAt(i);

            emit postItemAppended();
        }
        else{
            ++i;
        }
    }*/
}

