#include "messageSrc/message.h"

Message::Message(QString content, User* author, QDateTime date, QObject *parent) : QObject (parent)
{
    this->m_content = content;
    this->m_author = author;
    this->m_date = date;
}

QString Message::content() const
{
    return m_content;
}

User *Message::author() const
{
    return m_author;
}

QDateTime Message::date() const
{
    return m_date;
}


void Message::setContent(QString content)
{
    if (m_content == content)
        return;

    m_content = content;
    emit contentChanged(m_content);
}

void Message::setAuthor(User *author)
{
    if (m_author == author)
        return;

    m_author = author;
    emit authorChanged(m_author);
}

void Message::setDate(QDateTime date)
{
    if (m_date == date)
        return;

    m_date = date;
    emit dateChanged(m_date);
}
