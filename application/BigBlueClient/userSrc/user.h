#ifndef USER_H
#define USER_H

#include <QObject>

#include <QUrl>

class User : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nomUser READ nomUser WRITE setNomUser NOTIFY nomUserChanged)
    Q_PROPERTY(QUrl avatarUser READ avatarUser WRITE setAvatarUser NOTIFY avatarUserChanged)

    QString m_nomUser;
    QUrl m_avatarUser;

public:
    User(QUrl avatarUser, QString nomUser,QObject *parent=nullptr );


    QString nomUser() const;
    QUrl avatarUser() const;

signals:
    void preContentChanged();
    void postContentChanged();

    void preAuthorChanged();
    void postAuthorChanged();


    void nomUserChanged(QString nomUser);
    void avatarUserChanged(QUrl avatarUser);

public slots:
void setNomUser(QString nomUser);
void setAvatarUser(QUrl avatarUser);
};

#endif // USER_H
