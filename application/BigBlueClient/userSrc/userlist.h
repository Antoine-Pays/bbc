#ifndef USERLIST_H
#define USERLIST_H

#include "user.h"

#include <QObject>

class UserList : public QObject
{
    Q_OBJECT

private:
    QList<User*> m_listUser;

public:
    explicit UserList(std::list<User*> lc, QObject *parent = nullptr);

    QList<User*> getListUser();

    bool setItemAt(int index,const User &item);

signals:
    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem();
    void removeCompletItem();


};

#endif // USERLIST_H
