#ifndef USERMODEL_H
#define USERMODEL_H

#include <QAbstractListModel>

class UserList;

class UserModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(UserList *list READ list WRITE setList NOTIFY listChanged)

private:
    UserList *m_listUser;

public:
    explicit UserModel(QObject *parent = nullptr);

    enum{
        NomRole,
        AvatarRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int,QByteArray> roleNames() const override;

    UserList *list() const;

public slots:
    void setList(UserList *list) ;


signals:
    void listChanged(UserList * list);
};

#endif // USERMODEL_H
