import QtQuick 2.0

Rectangle {
    color:"#1F70F2"
    Image {

        id: servImg
        width: parent.width
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source:"qrc:/image/presentation.png"

    }
}
