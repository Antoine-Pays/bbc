#ifndef SERVERLIST_H
#define SERVERLIST_H

#include<QObject>
#include "server.h"



class ServerList : public QObject
{
    Q_OBJECT

private:
    QList<Server*> m_listServ;

public:
    explicit ServerList(std::list<Server*> lc, QObject *parent = nullptr);

    QList<Server*> getListServer();

    bool setItemAt(int index,const Server &item);

signals:
    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

    void itemChanged(int index);

public slots:
    void appendItem();
    void append(Server* item);
    void removeCompletItem();
    void removeItem(int index);
};



#endif // SERVERLIST_H
