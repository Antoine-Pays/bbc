#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QUrl>
#include "../roomSrc/roomlist.h"

class Server : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nomServer READ nomServer WRITE setNomServer NOTIFY nomServerChanged)
    Q_PROPERTY(RoomList *listRoom READ listRoom WRITE setListRoom NOTIFY listRoomChanged)
    Q_PROPERTY(QString lienServer READ lienServer WRITE setLienServer NOTIFY lienServerChanged)

    QString m_nomServer;
    RoomList * m_listRoom;
    QString m_lienServer;

public:
    Server(QString lienSource, QString nomServer, RoomList* listRoom, QObject *parent=nullptr );

    QString nomServer() const;
    RoomList * listRoom() const;
    QString lienServer() const;

signals:
    void preNomServChanged();
    void postNomServChanged();

    void preSourceImgChanged();
    void postSourceImgChanged();

    void nomServerChanged(QString nomServer);
    void listRoomChanged(RoomList * listRoom);
    void lienServerChanged(QString lienServer);

public slots :

    void setNomServer(QString nomServer);
    void setListRoom(RoomList * listRoom);
    void setLienServer(QString lienServer);
};

#endif // SERVER_H
