#ifndef SERVERMODEL_H
#define SERVERMODEL_H


#include <QAbstractListModel>

#include "roomSrc/roomlist.h"

#include "server.h"

class ServerList;

class ServerModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(ServerList *list READ list WRITE setList NOTIFY listChanged)
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY updateRoomModel)
private:
    ServerList *m_listServer;

    int m_currentIndex;

public:
    explicit ServerModel(QObject *parent = nullptr);

    enum{
        LienRole,
        NomRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int,QByteArray> roleNames() const override;

    ServerList *list() const;
    int currentIndex() const;

public slots:
    void setList(ServerList *list);
    void setCurrentIndex(int currentIndex);

signals:
    void listChanged(ServerList * list);
    void updateRoomModel(RoomList* r);
    void updateCurrentServer(Server *s);
};

#endif // SERVERMODEL_H
