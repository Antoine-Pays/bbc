import QtQuick 2.0
import QtMultimedia 5.4
import QtQuick.Controls 1.2

Item {

    Camera {
        id: cam
        captureMode: Camera.CaptureVideo
        videoRecorder.muted: false
        videoRecorder.mediaContainer: "mp4"
        //videoRecorder.outputLocation: "/tmp/toto.mp4"
    }
}

