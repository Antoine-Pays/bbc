#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc,argv);

    QQuickView view;
    QObject::connect(view.engine(), &QQmlEngine::quit, qApp, &QGuiApplication::quit);


    view.setSource(QUrl("qrc:/main.qml"));
    view.show();

    return app.exec();
}
