import QtQuick 2.0
import QtMultimedia 5.4
import QtQuick.Controls 2.0

Item {
    width: 1280
    height: 720

    ListView {
        id: devices
        height:  70

        model: QtMultimedia.availableCameras
        delegate: Text {
            text: modelData.displayName

            MouseArea {
                anchors.fill: parent
                onClicked: cam.deviceId = modelData.deviceId
            }
        }
    }


    Column{
        width: parent.width
        anchors.top: devices.bottom
        //anchors.bottom: video.top

        Row {
            spacing: 5

            Text {
                text: qsTr("Camera:")
            }
            Text {
                text:"Availibility " + cam.availability
            }
            Text {
                text: "State " + cam.cameraState
            }

            Text {
                text: "Status " + cam.cameraStatus
            }

        }
        Row {
            spacing: 5
            Label {
                text: qsTr("VideoRecorder")
            }
            Text {
                text: "Duration " + cam.videoRecorder.duration
            }
            Text {
                text: "State " + cam.videoRecorder.recorderState
            }
            Text {
                text: "Status " + cam.videoRecorder.recorderStatus
            }
            Text {
                text: "Location " + cam.videoRecorder.actualLocation
            }
            Text {
                text: "Error " + cam.videoRecorder.errorString
            }
        }

        Row {
            //anchors.bottom: item.top

            Column{
                id: videoCam
                VideoOutput {
                    source: cam
                    width: 200
                    height: 300
                }
            }

            Column{
                id: videoBuch
                VideoOutput {
                    source: bucheron
                    width: 200
                    height: 300
                }
            }

        }


        Row{
            Button {
                text: "Start Camera"
                onClicked: {
                    cam.start()
                    videoCam.opacity = 1.0
                    cam.videoRecorder.record()
                }
            }

            Button {
                text: "Stop Camera"
                onClicked: {
                    cam.stop()
                    videoCam.opacity = 0
                    cam.videoRecorder.stop()
                }
            }

            Button {
                text: "Start Audio/Media"
                onClicked: {
                    audioVideo.play()
                    bucheron.play()
                    videoBuch.opacity = 1.0
                }
            }

            Button {
                text: "Stop Audio/Media"
                onClicked: {
                    audioVideo.stop()
                    bucheron.stop()
                    videoBuch.opacity = 0
                }
            }
        }

        Row{
            TextArea {
                id: lienAudio
                placeholderText: qsTr("Enter description")
            }
        }

        Row {

            Slider {
                id: volumeSon
                from: 0
                to: 1
            }

        }
    }



    Camera {
        id: cam
        captureMode: Camera.CaptureVideo
        videoRecorder.muted: false
        videoRecorder.mediaContainer: "mp4"
        videoRecorder.outputLocation: "qrc:/Desktop/musique.mp3"
    }

    MediaPlayer {
        id: bucheron
        autoPlay: false
        muted: true
        source: "https://cdn.discordapp.com/attachments/758627025920851978/761634304798883840/V3B1Yq1x7ja8qS7nAPEvM68rzbnkNjIT.mp4"
        loops: MediaPlayer.Infinite
    }

    Audio {
        id: audioVideo
        autoPlay: false
        source: lienAudio.text
        loops: Audio.Infinite
        volume: volumeSon.value
    }
}
