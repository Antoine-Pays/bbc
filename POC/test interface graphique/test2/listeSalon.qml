import QtQuick 2.0

Rectangle {
    id: root
    //pour le designer :
    //width: 640
    //height: 400
    //le vrai programme:
    width: parent.width
    height: parent.height
    color: "#b7e6fb"
    Column{
        width: parent.width
        height: parent.height
        Rectangle{
            id: toolbar
            color: "grey"
            width: parent.width
            height: 50
            Image {
                id: name
                width: 40
                height: 40
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.margins: 10
                source: "qrc:/image/engrenage.png"
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        if(param.visible==true){
                            param.visible=false
                        }
                        else{
                            param.visible=true
                        }

                    }

                }
            }
        }

        ListView {
            id: listeSallon
            spacing: 10
            anchors.left: parent.left
            anchors.top: toolbar.bottom
            height: parent.height-toolbar.height
            width: 50
            clip: true
            model: salon{}

        }

        Parametre{
            id: param
            height: parent.height-toolbar.height
            width: parent.width
            anchors.bottom: parent.bottom
            color: "red"
            visible: false

        }


    }

}


/*##^##
Designer {
    D{i:0;formeditorZoom:0.6600000262260437}
}
##^##*/
