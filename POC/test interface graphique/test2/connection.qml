import QtQuick 2.0
import QtQuick.Controls 2.2
Rectangle {
    id: connection
    width: root.width
    height: root.height
    color: "#05abf6"


    Rectangle {
        color: "#fff"
        width: parent.width * 0.6
        height: parent.height * 0.5
        radius: 30
        anchors.centerIn: parent

        Column {
            width: parent.width * 0.9
            anchors.centerIn: parent
            id: column
            spacing: 5

            Rectangle {
                id: rec
                width: parent.width
                height: name.height

                Image {
                    id: name
                    anchors.horizontalCenter: rec.horizontalCenter
                    source: "qrc:/image/logo.png"
                    width: rec.width*0.3
                    fillMode: Image.PreserveAspectFit

                }
            }

            Rectangle {
                width: parent.width
                height: identifiant.height
                anchors.horizontalCenter: column.horizontalCenter
                Text {
                    id: identifiant
                    text: qsTr("Identifiant:")
                }
            }

            Rectangle {
                width: column.width*0.8
                height: 16
                border.color: "#000000"
                border.width: 1
                anchors.horizontalCenter: column.horizontalCenter
                TextInput {
                    id: tiIdentifiant
                    color: "#26282a"

                    horizontalAlignment: Text.AlignLeft
                    font.italic: true
                    selectedTextColor: "#26282a"
                }
            }

            Rectangle {

                width: parent.width
                height: password.height
                anchors.horizontalCenter: column.horizontalCenter
                Text {
                    id: password
                    text: qsTr("Mot de passe :")
                }
            }

            Rectangle {
                width: column.width*0.8
                height: 16
                border.color: "#000000"
                border.width: 1
                anchors.horizontalCenter: column.horizontalCenter
                TextInput {
                    id: tiPassword
                    font.italic: true
                }
            }

            Rectangle {
                width: 100
                height: 20
                radius: 20
                border.width: 1
                id: rectangle
                color: "#45c1f9"
                anchors.horizontalCenter: column.horizontalCenter
                Text {
                    id: text1
                    text: qsTr("Connection")
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 12
                    horizontalAlignment: Text.AlignLeft
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                MouseArea{
                    anchors.fill: parent
                        onClicked: {
                            var component = Qt.createComponent("listeSalon.qml")
                            var window = component.createObject(connection)
                            window.show()
                            Qt.quit()
                        }
                }
            }
        }
    }
}
