import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

import Server 1.0

Frame {
    ListView{
        implicitWidth: 150
        implicitHeight: root.height
        clip: true

        model: ServerModel{
            list: serverList
        }



        delegate: ItemDelegate{
            height: 60
            width: 90
            id: rowLayout
            Rectangle{
                id: recImage
                width: rowLayout.width
                Image {
                    source: model.source
                    width: parent.width
                    fillMode: Image.PreserveAspectFit
                }
            }

            Text {
                anchors.left: recImage.right
                Layout.fillWidth: true
                text: model.description
            }
        }
    }
}
