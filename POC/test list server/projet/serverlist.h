#ifndef SERVERLIST_H
#define SERVERLIST_H

#include<QObject>
#include<QUrl>
#include<server.h>


class ServerList : public QObject
{
    Q_OBJECT
public:
    //explicit ServerList();
    explicit ServerList(QObject *parent = nullptr);

    QList<Server*> getListServer();

    bool setItemAt(int index,const Server &item);

signals:
    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem();
    void removeCompletItem();

private:
    QList<Server*> listServ;
};



#endif // SERVERLIST_H
