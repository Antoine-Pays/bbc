#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QUrl>

class Server : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nomServer READ nomServer WRITE setNomServer NOTIFY nomServerChanged)
    Q_PROPERTY(QUrl sourceImg READ sourceImg WRITE setSourceImg NOTIFY sourceImgChanged)

    QString m_nomServer;
    QUrl m_sourceImg;

public:
    //Server(QObject *parent=nullptr);
    Server(QUrl sourceImg, QString nomServer,QObject *parent=nullptr );


    QString nomServer() const;

    QUrl sourceImg() const;

signals:
    void preNomServChanged();
    void postNomServChanged();

    void preSourceImgChanged();
    void postSourceImgChanged();

    void nomServerChanged(QString nomServer);

    void sourceImgChanged(QUrl sourceImg);

public slots :

void setNomServer(QString nomServer);
void setSourceImg(QUrl sourceImg);
};

#endif // SERVER_H
