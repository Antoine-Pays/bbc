import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import QtQuick.Controls 2.0

ApplicationWindow {
    id: root
    width: 640
    minimumWidth: 300
    height: 480
    minimumHeight: 400
    visible: true
    title: qsTr("BigBlueClient")


    ServerList{
        id:listeserv
    }
}
