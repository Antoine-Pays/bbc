#include "servermodel.h"
#include <QUrl>

#include "serverlist.h"

ServerModel::ServerModel(QObject *parent)
    : QAbstractListModel(parent)
    , listServ(nullptr)
{
}

int ServerModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !listServ)
        return 0;

    return listServ->getListServer().size();
}

QVariant ServerModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !listServ)
        return QVariant();
    const Server *item = listServ->getListServer().at(index.row());
    switch (role) {
        case SourceRole :
            return QVariant(item->sourceImg());
        case DescriptionRole :
            return QVariant(item->nomServer());
    }
    return QVariant();
}


bool ServerModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!listServ)
        return false;
    Server *item = listServ->getListServer()[index.row()];
    switch (role) {
        case SourceRole :
            item->setSourceImg(value.toUrl());
            break;
        case DescriptionRole :
            item->setNomServer(value.toString());
            break;
    }

    if (listServ->setItemAt(index.row(),*item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ServerModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> ServerModel::roleNames() const
{
     QHash<int, QByteArray> names;
     names[SourceRole]= "source";
     names[DescriptionRole]= "description";
     return names;
}

ServerList *ServerModel::list() const
{
    return listServ;
}

void ServerModel::setList(ServerList *list)
{
    beginResetModel();

    if(listServ)
        listServ->disconnect(this);

    listServ = list;

    if(listServ){
        connect(listServ, &ServerList::preItemAppended, this,[=](){
            const int index = listServ->getListServer().size();
            beginInsertRows(QModelIndex(),index, index);
        });
        connect(listServ, &ServerList::postItemAppended, this,[=](){
            endInsertRows();
        });
        connect(listServ, &ServerList::preItemRemoved, this,[=](int index){
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(listServ, &ServerList::postItemRemoved, this,[=](){
            endRemoveRows();
        });
    }
    endResetModel();
}
