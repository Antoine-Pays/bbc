#include "serverlist.h"


ServerList::ServerList(QObject *parent) : QObject(parent)
{
    listServ.append(new Server(QUrl("qrc:/image/logo.png"),"server test"));
    listServ.append(new Server(QUrl("qrc:/image/logo.png"),"server test2"));
    listServ.append(new Server(QUrl("qrc:/image/logo.png"),"server test3"));
    listServ.append(new Server(QUrl("qrc:/image/logo.png"),"server test4"));
    listServ.append(new Server(QUrl("qrc:/image/logo.png"),"server test5"));
}

QList<Server*> ServerList::getListServer() {
    return listServ;
}

bool ServerList::setItemAt(int index, const Server &item)
{
    if(index < 0 || index >= listServ.size())
        return false;
    const Server* oldItem = listServ.at(index);
    if(item.sourceImg() == oldItem->sourceImg()
            && item.nomServer() == oldItem->nomServer())
        return false;
    listServ[index]->setNomServer(QString(item.nomServer()));
    listServ[index]->setSourceImg(QUrl(item.sourceImg()));
    return true;
}

void ServerList::appendItem()
{
    emit preItemAppended();
    Server* item = new Server(QUrl(), nullptr, nullptr);
    listServ.append(item);

    emit postItemAppended();
}

void ServerList::removeCompletItem()
{
    /*for(int i =0; i< mItems.size(); ){
        if(mItems.at(i).source){
            emit preItemAppended();

            mItems.removeAt(i);

            emit postItemAppended();
        }
        else{
            ++i;
        }
    }*/
}
