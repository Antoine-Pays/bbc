#include "server.h"

/*Server::Server(){
    sourceImage=nullptr;
    this->nomServeur=nullptr;
}*/


Server::Server(QUrl sourceImg, QString nomServeur, QObject *parent) : QObject(parent)
{
    this->m_sourceImg=sourceImg;
    this->m_nomServer=nomServeur;
}

QString Server::nomServer() const
{
    return m_nomServer;
}

QUrl Server::sourceImg() const
{
    return m_sourceImg;
}

void Server::setNomServer(QString nomServer)
{
    if (m_nomServer == nomServer)
        return;

    m_nomServer = nomServer;
    emit nomServerChanged(m_nomServer);
}

void Server::setSourceImg(QUrl sourceImg)
{
    if (m_sourceImg == sourceImg)
        return;

    m_sourceImg = sourceImg;
    emit sourceImgChanged(m_sourceImg);
}
