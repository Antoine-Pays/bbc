#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "serverlist.h"
#include "servermodel.h"



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<ServerModel>("Server",1,0,"ServerModel");
    qmlRegisterUncreatableType<ServerList>("Server",1,0,"ServerList",
                                               QStringLiteral("ServerList should not be created in QML"));

    ServerList serverList;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QStringLiteral("serverList"),&serverList);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
