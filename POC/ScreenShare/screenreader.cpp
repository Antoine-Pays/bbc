#include "screenreader.h"
#include <QDebug>
#include <QObject>

//Notes:
// - format à définir pour la stdout du screen share (à voir avec les autres) doc:https://ffmpeg.org/ffmpeg-formats.html
//   VP8 ?
//   Processus à créer : processus qui enregistre c'est bon mais il manque celui qui lit en même temps, au lieu de tout enregistrer à la fin en MP4...
// (DONE) - buttons pour start/terminate (on linux terminate -> SIGSTOP)
// (DONE) - docu
// (DONE) - class ScreenRecorder qui wrap le QProcess et défini start & stop

ScreenReader::ScreenReader(QProcess *process, QString command, QStringList args)
{
    this->process = process;
    this->command = command;
    this->args = args;
}

/* function: start
 * functionality: start the QProcess
 * variables:   process : the QProcess
 *              command : ffmpeg
 *              args    : list of args of ffmpeg command
 */
void ScreenReader::start(){
    process->start(command, args);
    qDebug() << "Screen Record just started";
    qDebug() << process->arguments() << " ARGUMENTS";
    qDebug() << process->state() << " STATE";
    //        qDebug() << _FFMPEG->errorString();
    //        qDebug() << _FFMPEG->state() << " STATE";
    //        qDebug() << _FFMPEG->readAllStandardError() << " StdError";
    //        qDebug() << _FFMPEG->readAllStandardOutput() << " StdOutput";
    //        qDebug() << _FFMPEG->exitCode();
}

/* function: stop
 * functionality: terminate the QProcess
 * variables:   process : the QProcess
 */
void ScreenReader::stop()
{
    process->terminate();
    qDebug() << "Screen Record just terminated";
}
