#ifndef SCREENRECORDER_H
#define SCREENRECORDER_H

#include <QProcess>

class ScreenRecorder : public QObject
{
    Q_OBJECT
public:
    // QObjects are expected to support a parent/child hierarchy.  I've modified
    // the constructor to match the standard.
    explicit ScreenRecorder(QProcess *process, QString command, QStringList args);
private:
    QProcess *process;
    QString command;
    QStringList args;
    QProcess *child;
    QString child_command;
    QStringList child_args;
public slots:
    void start();
    void stop();
    //void start(QProcess *process, QString command, QStringList args);
    //void stop(QProcess *process);
private slots:
    void sendRead();
    void writeSomething();
    void handleError(QProcess::ProcessError error);
};

#endif // SCREENRECORDER_H
