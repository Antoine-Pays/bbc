QT += quick

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    screenrecorder.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# INCLUDEPATH += C:\Users\denis\Desktop\ffmpeg-2.1.3-mingw32pcx+msvc\include
#LIBS += -LC:\Users\denis\Desktop\ffmpeg-2.1.3-mingw32pcx+msvc\lib

INCLUDEPATH += /usr/include/x86_64-linux-gnu/libavcodec

LIBS += -lavformat  -lavcodec -lavutil -lswscale -lavutil -lm -lswresample

HEADERS += \
    wrapper.h \
    screenrecorder.h

