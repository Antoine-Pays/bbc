#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QProcess>
#include <QDir>
#include <QDebug>
#include <gst/gst.h>
#include <time.h>
#include <unistd.h>
#include <QQmlContext>
#include <QVariant>
#include "wrapper.h"
#include "screenrecorder.h"

void launch(QProcess *proc, QString str, QStringList strList){
    proc->start(str, strList);
}

/* ffmpeg -video_size 1920x1080 -framerate 30 -f x11grab -i :1 -y -t 00:10 /home/denis/Documents/qt/test.mp4
 * -i : display to catch
 * -y : rewrite access
 * -t : time format MM:SS
 */
// _paramListRecord << "-video_size" << "1920x1080" << "-framerate" << "30" << "-f" << "x11grab" << "-i" << ":1" << "-y" << "/home/denis/Documents/qt/test.mp4";
// << "-t" << "00:10" pour 10 secondes
int main(int argc, char *argv[])
{
    QProcess* _FFMPEG = new QProcess();
    QString _processRecord = "ffmpeg";
    QStringList _paramListRecord;
    _paramListRecord << "-video_size" << "1920x1080"
                     << "-framerate" << "30"
                     << "-f" << "x11grab"
                     << "-i" << ":1"
                     << "-y"
                     << "/home/denis/Documents/qt/test.mp4"
                     << "-f" << "matroska"
                     << "-";
    ScreenRecorder screenRecorder(_FFMPEG, _processRecord, _paramListRecord);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    engine.rootContext()->setContextProperty("screenRecorder", &screenRecorder);


    return app.exec();
}
