#ifndef SCREENREADERR_H
#define SCREENREADER_H

#include <QProcess>

class ScreenReader : public QObject
{
    Q_OBJECT
public:
    // QObjects are expected to support a parent/child hierarchy.  I've modified
    // the constructor to match the standard.
    explicit ScreenReader(QProcess *process, QString command, QStringList args);
private:
    QProcess *process;
    QString command;
    QStringList args;
public slots:
    void start();
    void stop();
    //void start(QProcess *process, QString command, QStringList args);
    //void stop(QProcess *process);
};

#endif // SCREENREADER_H
