import QtQuick 2.11
import QtQuick.Window 2.11

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    Rectangle {
        id: rectangle
        x: 0
        y: 0
        width: 325
        height: 130
        color: "#10f303"

        Text {
            id: element
            x: 130
            y: 47
            text: qsTr("Start")
            font.pixelSize: 30
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                // Call ScreenRecorder
                screenRecorder.start()
            }
        }
    }

    Rectangle {
        id: rectangle1
        x: 326
        y: 0
        width: 314
        height: 130
        color: "#ee1111"

        Text {
            id: element1
            x: 126
            y: 47
            text: qsTr("Stop")
            elide: Text.ElideRight
            font.pixelSize: 30
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                    screenRecorder.stop()
            }
        }
    }

    Rectangle {
       id: rectangle3
       x: 0
       y: rectangle1.height-1
       width: parent.width
       height: parent.height-rectangle1.height-1
       color: "#ee6661"

        
    }

}
