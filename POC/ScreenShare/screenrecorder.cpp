#include "screenrecorder.h"
#include <QDebug>
#include <QObject>

//Notes:
// - format à définir pour la stdout du screen share (à voir avec les autres) doc:https://ffmpeg.org/ffmpeg-formats.html
//   VP8 ?
//   Processus à créer : processus qui enregistre c'est bon mais il manque celui qui lit en même temps, au lieu de tout enregistrer à la fin en MP4...
// (DONE) - buttons pour start/terminate (on linux terminate -> SIGSTOP)
// (DONE) - docu
// (DONE) - class ScreenRecorder qui wrap le QProcess et défini start & stop

ScreenRecorder::ScreenRecorder(QProcess *process, QString command, QStringList args)
{
    this->process = process;
    this->command = command;
    this->args = args;
    //this->child = new QProcess(this->process);
    //connect(this->process, &QProcess::readyReadStandardOutput, this, &ScreenRecorder::sendRead);
    //connect(this->process, &QProcess::readyReadStandardError, this, &ScreenRecorder::sendRead);
    //connect(this->process, &QProcess::started, this, &ScreenRecorder::writeSomething);
    //child_args << "-";
    //child->setProcessChannelMode(QProcess::MergedChannels);
}

void ScreenRecorder::sendRead(){
    qDebug() << "data:" << child->readAllStandardError() << child->readAllStandardOutput();
}

void ScreenRecorder::writeSomething(){
//    qDebug() << "writing";
//    QString cmd = "read\n";
//    qint64 a = this->process->write(cmd.toStdString().c_str());
//    qDebug() << "wrote:" << a;
}

void ScreenRecorder::handleError(QProcess::ProcessError error){
    switch (error) {
        case QProcess::FailedToStart:
            qDebug() << "failed to start";
            break;
        case QProcess::Crashed:
            qDebug() << "crashed";
            break;
        default:
            break;
        }
}
//        qDebug() << _FFMPEG->errorString();
//        qDebug() << _FFMPEG->state() << " STATE";
//        qDebug() << _FFMPEG->readAllStandardError() << " StdError";
//        qDebug() << _FFMPEG->readAllStandardOutput() << " StdOutput";
//        qDebug() << _FFMPEG->exitCode();
//    child->start("ffplay",child_args);
/* function: start
 * functionality: start the QProcess
 * variables:   process : the QProcess
 *              command : ffmpeg
 *              args    : list of args of ffmpeg command
 */
void ScreenRecorder::start(){
    process->start(command, args);
    qDebug() << "Screen Record just started";
    qDebug() << process->arguments() << " ARGUMENTS";
    qDebug() << process->state() << " STATE";
}

/* function: stop
 * functionality: terminate the QProcess
 * variables:   process : the QProcess
 */
void ScreenRecorder::stop()
{
    process->terminate();
    qDebug() << "Screen Record just terminated";
}
