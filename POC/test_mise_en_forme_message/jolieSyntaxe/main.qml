import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import joliesyntaxe 1.0


Window {
    id: fenetre
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ColorDialog{
        id: colorDialog
        modality: Qt.WindowModal
        title: "Couleur"
        color: "black"
    }

    Rectangle{
        width: parent.width
        height: parent.height
        color: "grey"


        Rectangle{
            id: rec1
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top:parent.top
            anchors.topMargin: 20
            width: parent.width-10
            height: 100
            radius: 10
            JolieSyntaxe{
                id:jl2
            }
            TextEdit{
                id: textField
                //wrapMode: TextEdit.wrap
                textMargin: 2
                textFormat: TextEdit.RichText
                readOnly: true
            }
        }

        Rectangle {
            id: edition
            width: parent.width
            anchors.top:rec1.bottom; anchors.topMargin: 20
            Rectangle{
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width-10
                height: 40
                radius: 10
                JolieSyntaxe{
                    id:jl
                }
                TextEdit{
                    textMargin: 2
                    id:textField2
                    width: parent.width
                    height: parent.height
                    color: colorDialog.color
                    Component.onCompleted: {
                        jl.setDocument(textField2.textDocument)
                    }
                 }

            }
            Button{
                id:envoyer
                text: "envoyer"
                onClicked: {
                    textField.text=textField2.text
                    jl.setDocument(textField.textDocument)
                    textField2.text=""
                    jl2.setDocument(textField2.textDocument)
                }
            }

        }
    }
}
